/**
 * singleList.c
 */
#include <stdio.h>
#include <stdlib.h>
#include "singleList.h"
/////////////////////////////////////////////// cac ham
///////////////// cac ham khoi tao
// tao ds moi
void createSingleList(SingleList* list)
{
    (*list).root = (*list).prev = (*list).cur = (*list).tail = NULL;
}

// tao nut moi
Node* makeNewNode(ElementType e)
{
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->element = e;
    newNode->next = NULL;
    return newNode;
}

/////////////// insert
Node* insertEnd(SingleList* list, ElementType e)
{
    Node* newNode = makeNewNode(e);
    if ((*list).root == NULL) {
        (*list).root = (*list).tail = newNode;
    }
    else {
        (*list).tail->next = newNode;
        (*list).tail = newNode;
    }
    return (*list).tail;
}

// inser begin
Node* insertBegin(SingleList* list, ElementType e)
{
    Node* newNode = makeNewNode(e);
    if ((*list).root == NULL) {
        (*list).root = (*list).tail = newNode;
    }
    else {
        newNode->next = (*list).root;
        (*list).root = newNode;
    }
    return (*list).root;
}

// insert at  position
Node* insertAtPosition(SingleList* list, ElementType e, int n)
{
    Node* newNode = makeNewNode(e);
    if ((*list).root == NULL) {
        (*list).root = (*list).tail = (*list).cur = newNode;
    }
    else {
        if (n <= 1) {
            insertBegin(list, newNode->element);
            return (*list).cur;
        }
        if (n > totalSingleList(*list)) {
            insertEnd(list, newNode->element);
            return (*list).cur;
        }
        else {
            (*list).cur = (*list).prev = (*list).root;
            int i = 1;
            while (((*list).cur->next != NULL) && (i <= n - 1)) {
                i++;
                (*list).prev = (*list).cur;
                (*list).cur = (*list).cur->next;
            }
            newNode->next = (*list).cur;
            (*list).prev->next = (*list).cur = newNode;
        }
    }
    return (*list).cur;
}

//////////////////delete
//delete begin
Node* deleteBegin(SingleList* list)
{
    if ((*list).root != NULL) {
        Node* newNode = (*list).root;
        (*list).root = (*list).root->next;
        free(newNode);
    }
    return (*list).root;
}

// delete last
Node* deleteEnd(SingleList* list)
{
    if ((*list).root != NULL) {
        (*list).cur = (*list).prev = (*list).root;
        while (((*list).cur->next != NULL)) {
            (*list).prev = (*list).cur;
            (*list).cur = (*list).cur->next;
        }
        Node* newNode = (*list).cur;
        (*list).cur = (*list).cur->next;
        free(newNode);
        (*list).tail = (*list).prev;
        (*list).tail->next = NULL;
        return (*list).tail;
    }
    return NULL;
}

// delete all
Node* deleteSingleList(SingleList* list)
{
    while ((*list).root != NULL)
        deleteBegin(list);
    return NULL;
}

///////////////// total
int totalSingleList(SingleList list)
{
    int i = 0;
    list.cur = list.root;
    while (list.cur != NULL) {
        i++;
        list.cur = list.cur->next;
    }
    return i;
}
