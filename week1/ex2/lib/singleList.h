/**
 * singleList.h
 */
////////////////////////////////////
typedef struct
{
  // customize
  char studentCode[10];
  char lastName[20];
  char firstName[10];
  float middleMark;
  float endMark;
  char charMark;
} ElementType;
/////
typedef struct Node Node;
// mot Node gom
typedef struct Node {
    ElementType element;
    Node* next;
} Node;
// mot ds Node gom co root,prev,cur
typedef struct {
    Node* root;
    Node* cur;
    Node* prev;
    Node* tail;
} SingleList;

////////////////////////////////////////////////// nguyen ham
// cac ham khoi tao
void createSingleList(SingleList* list);
Node* makeNewNode(ElementType e);

// insert
Node* insertAfter(SingleList* list, ElementType e);
Node* insertBegin(SingleList* list, ElementType e);
Node* insertEnd(SingleList* list, ElementType e);
Node* insertAtPosition(SingleList* list, ElementType e, int n);

// delete
Node* deleteBegin(SingleList* list);
Node* deleteEnd(SingleList* list);
Node* deleteAtPosition(SingleList* list, int n);
Node* deleteSingleList(SingleList* list);

// total Node
int totalSingleList(SingleList list);
