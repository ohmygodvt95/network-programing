/**
 * Scoreboard.c
 * Le Vinh Thien
 */
#include "stdio.h"
#include "string.h"
#include "ctype.h"
#include "stdlib.h"
#include "lib/singleList.h"
#include "scoreboard.h"

char menu()
{
    char key;
    printf("-------------Learning Management System-------------\n");
    printf("1. Add a new score board\n");
    printf("2. Add score\n");
    printf("3. Remove score\n");
    printf("4. Search score\n");
    printf("5. Display score board and score report\n");
    printf("0. EXIT\n");
    printf("-------------------------------------\n");
    printf("-> Your choice (1-6, other to quit)?: ");
    scanf("%c%*c", &key);
    return key;
}

void menuOne()
{
    char key;
    subject sbj;
    int i;
    SingleList list;
    createSingleList(&list);
    do {
        printf("--> Subject information register: \n");
        printf("----> Subject ID: ");
        scanf("%[^\n]%*c", sbj.subjectId);
        printf("----> Subject Name: ");
        scanf("%[^\n]%*c", sbj.subject);
        printf("----> Subject middle mark: ");
        scanf("%d%*c", &sbj.middleMark);
        printf("----> Subject end mark: ");
        scanf("%d%*c", &sbj.endMark);
        printf("----> Subject semester: ");
        scanf("%[^\n]%*c", sbj.semester);
        printf("----> Total student: ");
        scanf("%d%*c", &sbj.totalStudent);
        for (i = 0; i < sbj.totalStudent; i++) {
            printf("------> Student information (%d/%d): \n", i + 1, sbj.totalStudent);
            insertBegin(&list, newStudent());
        }
        saveToFileInfo(sbj, list);
        deleteSingleList(&list);
        printf("----> continue? (y/N): ");
        scanf("%c%*c", &key);
    } while (toupper(key) == 'Y');
}
void menuTwo()
{
    char key;
    subject sbj;
    SingleList list;
    createSingleList(&list);
    do {
        printf("--> Subject information report: \n");
        printf("----> Subject ID: ");
        scanf("%[^\n]%*c", sbj.subjectId);
        printf("----> Subject semester: ");
        scanf("%[^\n]%*c", sbj.semester);
        FILE* f;
        f = fopen(fileName(sbj, FILE_DATA), "r");
        if (f == NULL) {
            printf("------> No information for %s\n", fileName(sbj, FILE_DATA));
        }
        else {
            int i;
            char s[100];
            fscanf(f, "%[^|]|%[^\n]\n", s, sbj.subjectId);
            fscanf(f, "%[^|]|%[^\n]\n", s, sbj.subject);
            fscanf(f, "%[^|]|%d|%d\n", s, &sbj.middleMark, &sbj.endMark);
            fscanf(f, "%[^|]|%[^\n]\n", s, sbj.semester);
            fscanf(f, "%[^|]|%d", s, &sbj.totalStudent);
            for (i = 0; i < sbj.totalStudent; i++) {
                ElementType e;
                fscanf(f, "\n%[^|]|%[^|]|%[^|]|%[^|]|%f|%f|%c", s, e.studentCode, e.lastName, e.firstName, &e.middleMark, &e.endMark, &e.charMark);
                insertBegin(&list, e);
            }
            printf("------> Add new student %s - %s\n", sbj.subjectId, sbj.semester);
            insertBegin(&list, newStudent());
            sbj.totalStudent++;
            saveToFileInfo(sbj, list);
            deleteSingleList(&list);
            fclose(f);
        }

        printf("----> continue? (y/N): ");
        scanf("%c%*c", &key);
    } while (toupper(key) == 'Y');
}
void menuThree()
{
    char key;
    subject sbj;
    SingleList list;
    createSingleList(&list);
    do {
        printf("--> Subject information report: \n");
        printf("----> Subject ID: ");
        scanf("%[^\n]%*c", sbj.subjectId);
        printf("----> Subject semester: ");
        scanf("%[^\n]%*c", sbj.semester);
        FILE* f;
        f = fopen(fileName(sbj, FILE_DATA), "r");
        if (f == NULL) {
            printf("------> No information for %s\n", fileName(sbj, FILE_DATA));
        }
        else {
            int i;
            char s[100];
            fscanf(f, "%[^|]|%[^\n]\n", s, sbj.subjectId);
            fscanf(f, "%[^|]|%[^\n]\n", s, sbj.subject);
            fscanf(f, "%[^|]|%d|%d\n", s, &sbj.middleMark, &sbj.endMark);
            fscanf(f, "%[^|]|%[^\n]\n", s, sbj.semester);
            fscanf(f, "%[^|]|%d", s, &sbj.totalStudent);
            for (i = 0; i < sbj.totalStudent; i++) {
                ElementType e;
                fscanf(f, "\n%[^|]|%[^|]|%[^|]|%[^|]|%f|%f|%c", s, e.studentCode, e.lastName, e.firstName, &e.middleMark, &e.endMark, &e.charMark);
                insertBegin(&list, e);
            }
            printf("------> Delete student in %s - %s by student code\n", sbj.subjectId, sbj.semester);
            ElementType e;
            printf("--------> Student code: ");
            scanf("%[^\n]%*c", e.studentCode);
            int p = indexStudent(list, e);
            if (p != 0) {
                deleteStudent(&list, p);
                sbj.totalStudent--;
                printf("------> Delete student %s success in %d\n", e.studentCode, p);
            }
            else
                printf("------> Not found student %s \n", e.studentCode);
            saveToFileInfo(sbj, list);
            deleteSingleList(&list);
            fclose(f);
        }

        printf("----> continue? (y/N): ");
        scanf("%c%*c", &key);
    } while (toupper(key) == 'Y');
}
int indexStudent(SingleList list, ElementType e)
{
    int i = 0;
    list.cur = list.root;
    while (list.cur != NULL) {
        ElementType v = list.cur->element;
        i++;
        if (strcmp(v.studentCode, e.studentCode) == 0)
            return i;
        list.cur = list.cur->next;
    }
    return 0;
}
Node* deleteStudent(SingleList* list, int p)
{
    int i = 1;
    if(p == 1){
        deleteBegin(list);
        return (*list).root;
    }
    (*list).cur = (*list).root;
    while (i != p - 1){
        (*list).cur = (*list).cur->next;
        i++;
    }
    Node* node = (*list).cur->next;

    (*list).cur->next = (*list).cur->next->next;
    free(node);
    return (*list).root;
}
void menuFour(){
    char key;
    subject sbj;
    SingleList list;
    createSingleList(&list);
    do {
        printf("--> Subject information report: \n");
        printf("----> Subject ID: ");
        scanf("%[^\n]%*c", sbj.subjectId);
        printf("----> Subject semester: ");
        scanf("%[^\n]%*c", sbj.semester);
        FILE* f;
        f = fopen(fileName(sbj, FILE_DATA), "r");
        if (f == NULL) {
            printf("------> No information for %s\n", fileName(sbj, FILE_DATA));
        }
        else {
            int i;
            char s[100];
            fscanf(f, "%[^|]|%[^\n]\n", s, sbj.subjectId);
            fscanf(f, "%[^|]|%[^\n]\n", s, sbj.subject);
            fscanf(f, "%[^|]|%d|%d\n", s, &sbj.middleMark, &sbj.endMark);
            fscanf(f, "%[^|]|%[^\n]\n", s, sbj.semester);
            fscanf(f, "%[^|]|%d", s, &sbj.totalStudent);
            for (i = 0; i < sbj.totalStudent; i++) {
                ElementType e;
                fscanf(f, "\n%[^|]|%[^|]|%[^|]|%[^|]|%f|%f|%c", s, e.studentCode, e.lastName, e.firstName, &e.middleMark, &e.endMark, &e.charMark);
                insertBegin(&list, e);
            }
            printf("------> Find student in %s - %s by student code\n", sbj.subjectId, sbj.semester);
            ElementType e;
            printf("--------> Student code: ");
            scanf("%[^\n]%*c", e.studentCode);
            ElementType ee;
            ee.studentCode[0] = '\0';
            list.cur = list.root;
            while (list.cur != NULL) {
                ElementType v = list.cur->element;
                if (strcmp(v.studentCode, e.studentCode) == 0){
                    ee = v;
                    break;
                }
                list.cur = list.cur->next;
            }
            if (strlen(ee.studentCode) != 0) {
                printf("------> Found student %s\n", ee.studentCode);
                printf("%s|%s|%s|%s|%0.1f|%0.1f|%c\n", "S", ee.studentCode, ee.lastName, ee.firstName, ee.middleMark, ee.endMark, ee.charMark);
            }
            else
                printf("------> Not found student %s\n", e.studentCode);
            saveToFileInfo(sbj, list);
            deleteSingleList(&list);
            fclose(f);
        }

        printf("----> continue? (y/N): ");
        scanf("%c%*c", &key);
    } while (toupper(key) == 'Y');
}

void menuFive()
{
    char key;
    subject sbj;
    SingleList list;
    createSingleList(&list);
    do {
        printf("--> Subject information report: \n");
        printf("----> Subject ID: ");
        scanf("%[^\n]%*c", sbj.subjectId);
        printf("----> Subject semester: ");
        scanf("%[^\n]%*c", sbj.semester);
        FILE* f;
        f = fopen(fileName(sbj, FILE_DATA), "r");
        if (f == NULL) {
            printf("------> No information for %s\n", fileName(sbj, FILE_DATA));
        }
        else {
            int a[5] = { 0, 0, 0, 0, 0 };
            ElementType max;
            max.middleMark = 0;
            max.endMark = 0;
            ElementType min;
            min.middleMark = 10;
            min.endMark = 10;
            float sum = 0;
            int i;
            char s[100];
            fscanf(f, "%[^|]|%[^\n]\n", s, sbj.subjectId);
            fscanf(f, "%[^|]|%[^\n]\n", s, sbj.subject);
            fscanf(f, "%[^|]|%d|%d\n", s, &sbj.middleMark, &sbj.endMark);
            fscanf(f, "%[^|]|%[^\n]\n", s, sbj.semester);
            fscanf(f, "%[^|]|%d", s, &sbj.totalStudent);
            for (i = 0; i < sbj.totalStudent; i++) {
                ElementType e;
                fscanf(f, "\n%[^|]|%[^|]|%[^|]|%[^|]|%f|%f|%c", s, e.studentCode, e.lastName, e.firstName, &e.middleMark, &e.endMark, &e.charMark);
                insertBegin(&list, e);
            }
            printf("%s--------------------------\n", fileName(sbj, FILE_DATA));
            printf("%s|%s\n", "SubjectID", sbj.subjectId);
            printf("%s|%s\n", "Subject", sbj.subject);
            printf("%s|%d|%d\n", "F", sbj.middleMark, sbj.endMark);
            printf("%s|%s\n", "Semester", sbj.semester);
            printf("%s|%d\n", "StudentCount", sbj.totalStudent);
            list.cur = list.root;
            while (list.cur != NULL) {
                ElementType e = list.cur->element;
                printf("%s|%s|%s|%s|%0.1f|%0.1f|%c\n", "S", e.studentCode, e.lastName, e.firstName, e.middleMark, e.endMark, e.charMark);
                sum += e.middleMark * sbj.middleMark + e.endMark * sbj.endMark;

                if ((e.middleMark * sbj.middleMark + e.endMark * sbj.endMark)
                    > (max.middleMark * sbj.middleMark + max.endMark * sbj.endMark))
                    max = e;

                if ((e.middleMark * sbj.middleMark + e.endMark * sbj.endMark)
                    < (min.middleMark * sbj.middleMark + min.endMark * sbj.endMark))
                    min = e;
                switch (e.charMark) {
                case 'A':
                    a[0]++;
                    break;
                case 'B':
                    a[1]++;
                    break;
                case 'C':
                    a[2]++;
                    break;
                case 'D':
                    a[3]++;
                    break;
                case 'F':
                    a[4]++;
                    break;
                }
                list.cur = list.cur->next;
            }
            printf("_______________________________________________________________________\n");
            saveToFileReport(sbj, max, min, sum / (sbj.middleMark + sbj.endMark) / sbj.totalStudent, a);
            deleteSingleList(&list);
            fclose(f);
        }

        printf("----> continue? (y/N): ");
        scanf("%c%*c", &key);
    } while (toupper(key) == 'Y');
}

void saveToFileInfo(subject sbj, SingleList list)
{
    FILE* f;
    f = fopen(fileName(sbj, FILE_DATA), "w+");
    fprintf(f, "%s|%s\n", "SubjectID", sbj.subjectId);
    fprintf(f, "%s|%s\n", "Subject", sbj.subject);
    fprintf(f, "%s|%d|%d\n", "F", sbj.middleMark, sbj.endMark);
    fprintf(f, "%s|%s\n", "Semester", sbj.semester);
    fprintf(f, "%s|%d", "StudentCount", sbj.totalStudent);
    list.cur = list.root;
    while (list.cur != NULL) {
        ElementType e = list.cur->element;
        fprintf(f, "\n%s|%s|%s|%s|%0.1f|%0.1f|%c", "S", e.studentCode, e.lastName, e.firstName, e.middleMark, e.endMark, pointToChar(e.middleMark, e.endMark, sbj));
        list.cur = list.cur->next;
    }
    fclose(f);
}
void saveToFileReport(subject sbj, ElementType max, ElementType min, float avg, int analyst[])
{

    FILE* f;
    f = fopen(fileName(sbj, FILE_REPORT), "w+");
    printf("%s--------------------------\n", fileName(sbj, FILE_REPORT));
    printf("%s %s %s\n", "The student with the highest mark is: ", max.lastName, max.firstName);
    printf("%s %s %s\n", "The student with the lowest mark is:", min.lastName, min.firstName);
    printf("%s %0.2f\n", "The average mark is: ", avg);
    printf("A histogram of the subject %s is:\n", sbj.subjectId);
    printf("A: %s\n", progess(analyst[0]));
    printf("B: %s\n", progess(analyst[1]));
    printf("C: %s\n", progess(analyst[2]));
    printf("D: %s\n", progess(analyst[3]));
    printf("F: %s\n", progess(analyst[4]));
    printf("________________________________________________________________________\n");

    fprintf(f, "%s%s %s\n", "The student with the highest mark is: ", max.lastName, max.firstName);
    fprintf(f, "%s%s %s\n", "The student with the lowest mark is:", min.lastName, min.firstName);
    fprintf(f, "%s%f\n", "The average mark is: ", avg);
    fprintf(f, "A histogram of the subject %s is:\n", sbj.subjectId);
    fprintf(f, "A:%s\n", progess(analyst[0]));
    fprintf(f, "B:%s\n", progess(analyst[1]));
    fprintf(f, "C:%s\n", progess(analyst[2]));
    fprintf(f, "D:%s\n", progess(analyst[3]));
    fprintf(f, "F:%s\n", progess(analyst[4]));

    fclose(f);
}
char* progess(int a)
{
    char* s;
    int i = 0;
    s = (char*)malloc(sizeof(char) * 255);
    s[0] = '\0';
    while (i < a) {
        strcat(s, "*");
        i++;
    }
    return s;
}
ElementType newStudent()
{
    ElementType e;
    printf("--------> Student code: ");
    scanf("%[^\n]%*c", e.studentCode);
    printf("--------> Last name: ");
    scanf("%[^\n]%*c", e.lastName);
    printf("--------> First name: ");
    scanf("%[^\n]%*c", e.firstName);
    printf("--------> Middle mark: ");
    scanf("%f%*c", &e.middleMark);
    printf("--------> End mark: ");
    scanf("%f%*c", &e.endMark);
    return e;
}
char pointToChar(float middle, float end, subject sbj)
{
    float avgMark = (middle * sbj.middleMark + end * sbj.endMark) / (sbj.middleMark + sbj.endMark);
    if (avgMark >= 8.5 && avgMark <= 10)
        return 'A';
    else if (avgMark >= 7 && avgMark < 8.5)
        return 'B';
    else if (avgMark >= 5.5 && avgMark < 7)
        return 'C';
    else if (avgMark >= 4 && avgMark < 5.5)
        return 'D';
    else
        return 'F';
}
char* fileName(subject sbj, int isReport)
{
    char* s;
    s = (char*)malloc(sizeof(char) * 50);
    s[0] = '\0';
    strcat(s, "data/");
    strcat(s, sbj.subjectId);
    strcat(s, "_");
    strcat(s, sbj.semester);
    if (isReport == FILE_REPORT) {
        strcat(s, "_rp");
    }
    strcat(s, ".txt");
    return s;
}
int main(int argc, char const* argv[])
{
    char key;
    do {
        key = menu();
        switch (key) {
        case '1':
            menuOne();
            break;
        case '2':
            menuTwo();
        case '3':
            menuThree();
            break;
        case '4':
            menuFour();
            break;
        case '5':
            menuFive();
            break;
        }
    } while (key != '0');
    return 0;
}
