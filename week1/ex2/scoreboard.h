/**
 * Scoreboard.h
 * Le Vinh Thien
 */
#ifndef FILE_DATA
#define FILE_DATA 0x0001
#endif
#ifndef FILE_REPORT
#define FILE_REPORT 0x0010
#endif
typedef struct{
	char subjectId[10];
	char subject[50];
	int middleMark;
	int endMark;
	char semester[10];
	int totalStudent;
} subject;

char menu();
void menuOne();
void menuTwo();
void menuThree();
void menuFour();
void menuFive();
char * progess(int a);
subject init();
ElementType newStudent();
char pointToChar(float middle, float end, subject sbj);
char * fileName(subject sbj, int isReport);
void saveToFileInfo(subject sbj, SingleList list);
void saveToFileReport(subject sbj, ElementType max, ElementType min, float avg, int analyst[]);
Node * deleteStudent(SingleList * list, int p);
int indexStudent(SingleList list, ElementType e);
