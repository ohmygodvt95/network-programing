/**
 * HAZCHEM.h
 * Le Vinh Thien - 20133740
 * HEDSPI K58
 */
#ifndef FIRST_VALUE
#define FIRST_VALUE "1234"
#endif
#ifndef SECOND_VALUE
#define SECOND_VALUE "PRSTWXYZ"
#endif
#ifndef SECOND_VALUE_ADD
#define SECOND_VALUE_ADD "STYZ"
#endif
#ifndef THIRD_VALUE
#define THIRD_VALUE "E"
#endif
typedef struct
{
	char code[3];
	int isColor;
} HazchemCode;
typedef struct
{
	char M1[100];
	char M2[100];
	char M3[100];
	char M4[100];
	char V[100];
	char FULL[100];
	char BA[100];
	char BAO[100];
	char DILUTE[100];
	char CONTAIN[100];
	char E[100];
	char N[100];
} Info;
Info info;
Info init();
char * toUpperString(char s[]);
int isContrainArray(char c, char s[]);
int isHazchemCode(char s[]);
char * material(HazchemCode hazchemCode);
char * reactivity(HazchemCode hazchemCode);
char * protection(HazchemCode hazchemCode);
char * containment(HazchemCode hazchemCode);
