/**
 * HAZCHEM.c
 * Le Vinh Thien - 20133740
 * HEDSPI K58
 */
#include "stdio.h"
#include "ctype.h"
#include "string.h"
#include "HAZCHEM.h"
Info init(){
	Info i;
	strcpy(i.M1, "Jets");
	strcpy(i.M2, "Fog");
	strcpy(i.M3, "Foam");
	strcpy(i.M4, "Dry agent");
	strcpy(i.V, "Co the phan ung manh");
	strcpy(i.FULL, "Bao ve toan than voi quan ao va mat na");
	strcpy(i.BA, "Can mat na phong doc va gang tay");
	strcpy(i.BAO, "Can mat na phong doc va gang tay chi doi voi lua");
	strcpy(i.DILUTE, "Hoa chat co the duoc pha loang voi nuoc va rua sach");
	strcpy(i.CONTAIN, "Hoa chat phai dung trong vat chua");
	strcpy(i.E, "Can so tan");
	strcpy(i.N, "");
	return i;
}
char * toUpperString(char s[]){
	int i;
	for(i = 0; i < strlen(s); i++){
		s[i] = toupper(s[i]);
	}
	return s;
}
int isContrainArray(char c, char s[]){
	int i;
	for(i = 0; i < strlen(s); i++){
		if(c == s[i])
			return 1;
	}
	return 0;
}
int isHazchemCode(char s[]){
	if(strlen(s) < 2 || strlen(s) > 3)
		return 0;
	if(!isContrainArray(toupper(s[0]), FIRST_VALUE))
		return 0;
	if(!isContrainArray(toupper(s[1]), SECOND_VALUE))
		return 0;
	if(strlen(s) == 3){
		if(!isContrainArray(toupper(s[2]), THIRD_VALUE))
			return 0;
	}
	return 1;
}
char * material(HazchemCode hazchemCode){
	switch(hazchemCode.code[0]){
		case '1': return info.M1;
		case '2': return info.M2;
		case '3': return info.M3;
		case '4': return info.M4;
	}
	return NULL;
}
char * reactivity(HazchemCode hazchemCode){
	switch(hazchemCode.code[1]){
		case 'P': return info.V; break;
		case 'S': return info.V; break;
		case 'W': return info.V; break;
		case 'Y': return info.V; break;
		case 'Z': return info.V; break;
	}
	return info.N;
}
char * protection(HazchemCode hazchemCode){
	switch(hazchemCode.code[1]){
		case 'P': return info.FULL; break;
		case 'R': return info.FULL; break;
		case 'S':
			if(hazchemCode.isColor == 0){
				return info.BA;
			}
			else return info.BAO;
			break;
		case 'T':
			if(hazchemCode.isColor == 0){
				return info.BA;
			}
			else return info.BAO;
			break;
		case 'W': return info.FULL; break;
		case 'Y':
			if(hazchemCode.isColor == 0){
				return info.BA;
			}
			else return info.BAO;
			break;
		case 'Z':
			if(hazchemCode.isColor == 0){
				return info.BA;
			}
			else return info.BAO;
			break;
	}
	return info.N;
}
char *containment(HazchemCode hazchemCode){
	switch(hazchemCode.code[1]){
		case 'P': return info.DILUTE;
		case 'R': return info.DILUTE;
		case 'S': return info.DILUTE;
		case 'T': return info.DILUTE;
		case 'W': return info.CONTAIN;
		case 'X': return info.CONTAIN;
		case 'Y': return info.CONTAIN;
		case 'Z': return info.CONTAIN;
	}
	return NULL;
}
char * ce(HazchemCode hazchemCode){
	return info.E;
}
int main(int argc, char const *argv[])
{
	info = init();
	HazchemCode hazchemCode;
	int flag = 0;
	do{
		printf("HAZCHEM code: ");
		scanf("%[^\n]%*c", hazchemCode.code);
		strcpy(hazchemCode.code, toUpperString(hazchemCode.code));
		flag = isHazchemCode(hazchemCode.code);
		if(flag == 1 && isContrainArray(hazchemCode.code[1], SECOND_VALUE_ADD) == 1){
			char isColor[3];
			printf("Is the %c reverse coloured?(yes/no): ", hazchemCode.code[1]);
			scanf("%[^\n]%*c", isColor);
			if(strcmp(toUpperString(isColor), "YES") == 0){
				hazchemCode.isColor = 1;
			}
			else hazchemCode.isColor = 0;
		}
		else{
			printf("WARNING: HazchemCode was Wrong! Please try again!\n");
		}
	} while(flag == 0);
	printf("*************Emergency action advice************\n");
	printf("Material: %s\n", material(hazchemCode));
	printf("Reactivity: %s\n", reactivity(hazchemCode));
	printf("Protection: %s\n", protection(hazchemCode));
	printf("Contaiment: %s\n", containment(hazchemCode));
	printf("Evacuation: %s\n", ce(hazchemCode));
	printf("************************************************\n");
	return 0;
}
