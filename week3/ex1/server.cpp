//
// Created by lengkeng on 21/09/2016.
//
#include <iostream>
#include <stdio.h>          /* These are the usual header files */
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <zconf.h>

#define PORT 5500   /* Port that will be opened */
#define BACKLOG 2   /* Number of allowed connections */

int main()
{

    int listen_sock, conn_sock; /* file descriptors */
    char recv_data[1024];
    int bytes_sent, bytes_received;
    struct sockaddr_in server; /* server's address information */
    struct sockaddr_in client; /* client's address information */

    int sin_size;

    if ((listen_sock=socket(AF_INET, SOCK_STREAM, 0)) == -1 ){  /* calls socket() */
        printf("socket() error\n");
        return (-1);
    }

    server.sin_family = AF_INET;
    server.sin_port = htons(PORT);   /* Remember htons() from "Conversions" section? =) */
    server.sin_addr.s_addr = INADDR_ANY;  /* INADDR_ANY puts your IP address automatically */
    bzero(&(server.sin_zero),8); /* zero the rest of the structure */


    if(bind(listen_sock,(struct sockaddr*)&server,sizeof(struct sockaddr))==-1){ /* calls bind() */
        printf("bind() error\n");
        return(-1);
    }

    if(listen(listen_sock,BACKLOG) == -1){  /* calls listen() */
        printf("listen() error\n");
        return(-1);
    }

    while(1){
        sin_size=sizeof(struct sockaddr_in);
        if ((conn_sock = accept(listen_sock, (struct sockaddr *)&client, (socklen_t *) &sin_size)) == -1){ /* calls accept() */
            printf("accept() error\n");
            return(-1);
        }

        printf("You got a connection from %s\n",inet_ntoa(client.sin_addr) ); /* prints client's IP */

        bytes_sent = send(conn_sock,"Welcome to my server.\n", 1024,0); /* send to the client welcome message */
        if (bytes_sent < 0){
            printf("\nError!Can not sent data to client!");
            close(conn_sock);
            continue;
        }

        while(bytes_received = recv(conn_sock,recv_data,1024,0)){
            recv_data[bytes_received] = '\0';

            for(int i = 0; i < strlen(recv_data); i++) recv_data[i] = toupper(recv_data[i]);
            puts(recv_data);
            bytes_sent = send(conn_sock, recv_data, 1024,0); /* send to the client welcome message */
            if (bytes_sent < 0){
                printf("\nError!Can not sent data to client!");
                close(conn_sock);
                continue;
            }
            if(strcmp(recv_data, "Q") == 0) {
                break;
            }
        }

    }

    printf("\nEXIT!Can not receive data from client!");
    close(conn_sock);
    return 0;
}