cmake_minimum_required(VERSION 3.6)
project(week6)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES ex2/server.cpp)
add_executable(ex2_server ${SOURCE_FILES})
set(SOURCE_FILES ex2/client.cpp ex2/md5.cpp ex2/md5.h)
add_executable(ex2_client ${SOURCE_FILES})

set(SOURCE_FILES ex3/server.cpp)
add_executable(ex3_server ${SOURCE_FILES})
set(SOURCE_FILES ex3/client.cpp ex3/md5.cpp ex3/md5.h)
add_executable(ex3_client ${SOURCE_FILES})

set(SOURCE_FILES ex1/server.cpp)
add_executable(ex1_server ${SOURCE_FILES})
set(SOURCE_FILES ex1/client.cpp)
add_executable(ex1_client ${SOURCE_FILES})