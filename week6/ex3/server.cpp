//
//  Le Vinh Thien 12/10/2016
//
#include <iostream>
#include <string.h>   //strlen
#include <unistd.h>   //close
#include <arpa/inet.h>    //close
#include <poll.h>
#include <signal.h>
#include <asm/ioctls.h>
#include <sys/ioctl.h>

using namespace std;
#define TRUE   1
#define FALSE  0
#define PORT 5500
#define SIGN_SUCCESS "success"
#define SIGN_FAILURE "failure"
#define SIGN_LOCKED "locked"
#define MAX 32

int check_exits_username(char s[]) {
    FILE *f;
    char user[100];
    char pass[100];
    f = fopen("users.txt", "r+");
    if (f == NULL) return 0;
    while (!feof(f)) {
        if (fscanf(f, "%s %s\n", user, pass) > 1) {
            if (strcmp(user, s) == 0) {
                fclose(f);
                return 1;
            }
        }
    }
    fclose(f);
    return 0;
}

int check_username_password(char u[], char p[]) {
    FILE *f;
    char user[100];
    char pass[100];
    f = fopen("users.txt", "r+");
    if (f == NULL) return 0;
    while (!feof(f)) {
        if (fscanf(f, "%s %s\n", user, pass) > 1) {
            if (strcmp(u, user) == 0 && strcmp(p, pass) == 0) {
                fclose(f);
                return 1;
            }
        }
    }
    fclose(f);
    return 0;
}

int main(int argc, char *argv[]) {
    int opt = TRUE;
    int master_socket, addrlen;
    int max_sd;
    struct sockaddr_in address;
    struct pollfd poll_set[100];
    int numfds = 0;
    int client[MAX] = {0};
    int status[100];
    int locked[100];
    char users[100][100];
    //set of socket descriptors
    fd_set readfds;

    char message[] = "Welcome my server";

    //create a master socket
    if ((master_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    //set master socket to allow multiple connections , this is just a good habit, it will work without this
    if (setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *) &opt, sizeof(opt)) < 0) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    //type of socket created
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    //bind the socket to localhost port 5500
    if (bind(master_socket, (struct sockaddr *) &address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    printf("Listener on port %d \n", PORT);

    //try to specify maximum of 5 pending connections for the master socket
    if (listen(master_socket, 5) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    //accept the incoming connection
    addrlen = sizeof(address);
    poll_set[0].fd = master_socket;
    poll_set[0].events = POLLIN;
    numfds++;

    while (TRUE) {
        char buff[1024];
        int fd_index;
        int bytes_received;
        int bytes_sent;
        int client_sockfd;
        int total = 0;
        for (int i = 1; i < MAX; i++) if (client[i] != 0) total++;
        printf("Waiting for client (%d total)...\n", total);
        poll(poll_set, MAX, 3500);
        for (fd_index = 0; fd_index < MAX; fd_index++) {
            if (poll_set[fd_index].revents & POLLIN) {
                if (poll_set[fd_index].fd == master_socket) {
                    client_sockfd = accept(master_socket, (struct sockaddr *) &address, (socklen_t *) &addrlen);
                    for (int i = 1; i < MAX; i++) {
                        if (client[i] == 0) {
                            client[i] = client_sockfd;
                            poll_set[i].fd = client_sockfd;
                            poll_set[i].events = POLLIN;
                            status[i] = 0;
                            locked[i] = 5;
                            break;
                        }
                    }


                    printf("Adding client on fd %d\n", client_sockfd);
                    if (send(client_sockfd, message, strlen(message), 0) != strlen(message)) {
                        perror("send");
                    }
                } else {
                    ioctl(poll_set[fd_index].fd, FIONREAD, &bytes_received);
                    char buffer[1025];
                    int sd = poll_set[fd_index].fd;
                    //Check if it was for closing , and also read the incoming message
                    if (status[fd_index] == 0) {
                        bytes_received = recv(sd, buffer, 1024, 0);
                        if (bytes_received < 0) {
                            printf("\nError!Cannot receive data from client!\n");
                            numfds--;
                            poll_set[fd_index].events = 0;
                            printf("Removing client on fd %d\n", poll_set[fd_index].fd);
                            poll_set[fd_index].fd = -1;
                            client[fd_index] = 0;
                            status[fd_index] = 0;
                            locked[fd_index] = 5;
                        } else {
                            buffer[bytes_received] = '\0';
                            if (check_exits_username(buffer)) {
                                status[fd_index] = 1;
                                strcpy(users[fd_index], buffer);
                                bytes_sent = send(sd, SIGN_SUCCESS, strlen(SIGN_SUCCESS), 0);
                            } else {
                                bytes_sent = send(sd, SIGN_FAILURE, strlen(SIGN_FAILURE), 0);
                            }
                            if (bytes_sent < 0) {
                                printf("\nError!Can not sent data to client!");
                                close(sd);
                                continue;
                            }
                        }
                    } else if (status[fd_index] == 1) {
                        bytes_received = recv(sd, buffer, 1024, 0);
                        if (bytes_received < 0) {
                            printf("\nError!Cannot receive data from sever!\n");
                            close(sd);
                            exit(0);
                        } else {
                            buffer[bytes_received] = '\0';
                            if (check_username_password(users[fd_index], buffer)) {
                                status[fd_index] = 2;
                                bytes_sent = send(sd, SIGN_SUCCESS, strlen(SIGN_SUCCESS), 0);
                                cout << users[fd_index] << " authenticated" << endl;
                            } else {
                                if (--locked[fd_index] != 1)
                                    bytes_sent = send(sd, SIGN_FAILURE, strlen(SIGN_FAILURE), 0);
                                else {
                                    status[fd_index] = 3;
                                    bytes_sent = send(sd, SIGN_LOCKED, strlen(SIGN_LOCKED), 0);
                                }
                            }
                            if (bytes_sent < 0) {
                                printf("\nError!Can not sent data to client!");
                                close(sd);
                                continue;
                            }
                        }
                    } else {
                        bytes_received = recv(sd, buffer, 3, 0);
                        getpeername(sd, (struct sockaddr *) &address, (socklen_t *) &addrlen);
                        printf("Host disconnected , ip %s , port %d \n", inet_ntoa(address.sin_addr),
                               ntohs(address.sin_port));
                        // reset
                        close(sd);
                        poll_set[fd_index].fd = -1;
                        client[fd_index] = 0;
                        status[fd_index] = 0;
                        locked[fd_index] = 0;
                    }
                }
            }
        }
    }


    return 0;
}