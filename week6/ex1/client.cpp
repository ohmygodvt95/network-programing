//
// Created by lengkeng on 27/09/2016.
//
#include <iostream>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <zconf.h>
using namespace std;
int main(){
    int client_sock;
    char buff[1024];
    FILE *f;
    struct sockaddr_in server_addr;
    int bytes_sent,bytes_received;

    client_sock=socket(AF_INET,SOCK_STREAM,0);

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(5500);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

    if(connect(client_sock,(struct sockaddr*)&server_addr,sizeof(struct sockaddr))!=0){
        printf("\nError!Can not connect to sever!Client exit imediately! ");
        return 0;
    }

    bytes_received = recv(client_sock, buff, 1024, 0);
    if(bytes_received == -1){
        printf("\nError!Cannot receive data from sever!\n");
        close(client_sock);
        return(-1);
    }else{
        buff[bytes_received] = '\0';
        puts(buff);
        buff[0] = '\0';
        long total = 0;
        int count = 0;
        char file_name[100];
        printf("\nPath file data:");
        gets(file_name);
        f = fopen(file_name,"r+");
        if(f == NULL){
            printf("\nFile not exits");
            return 1;
        }
        printf("\nSending...");
        while(!feof(f)){
            buff[0] = '\0';
            count = fread(buff, sizeof(char), 1024, f);
            buff[count] = '\0';
            bytes_sent = send(client_sock,buff,strlen(buff),0);
            if (bytes_sent == 0) {
                printf("\nError!Can not sent data to client!");
                close(client_sock);
                continue;
            }
        }
        rewind(f);
        printf("\n=>Success");
        printf("\nRecv..\n");
        bytes_received = recv(client_sock,buff,1024,0);
        bytes_sent = send(client_sock,"OK", 3, 0);
        while(bytes_received = recv(client_sock,buff,1024,0)){
            buff[bytes_received] = '\0';
            for(int i = 0; i < strlen(buff); i++) buff[i] = toupper(buff[i]);
            fputs(buff, f);
            total += bytes_received;
            if(bytes_received != 1024) break;
            bytes_sent = send(client_sock,"OK", 3, 0);
        }
        fclose(f);
        printf("\n->finish: %li bytes", total);
    }

    close(client_sock);
    return 0;
}