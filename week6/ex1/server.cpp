//
// Created by lengkeng on 23/10/2016.
//
#include <iostream>
#include <string.h>   //strlen
#include <unistd.h>   //close
#include <arpa/inet.h>    //close
#include <poll.h>
#include <asm/ioctls.h>
#include <sys/ioctl.h>

using namespace std;
#define TRUE   1
#define FALSE  0
#define PORT 5500
#define SIGN_SUCCESS "success"
#define SIGN_FAILURE "failure"
#define SIGN_LOCKED "locked"
#define MAX 32
char * get_file_name(int i){
    char *s;
    s = (char *)malloc(sizeof(char) *20);
    strcpy(s, "tmp_ .txt\0");
    s[4] = char(48 + i);
    return s;
}
int main(int argc, char *argv[]) {
    int opt = TRUE;
    int master_socket, addrlen;
    struct sockaddr_in address;
    struct pollfd poll_set[100];
    int client[MAX] = {0};
    int status[100];
    FILE *f[100];


    char message[] = "Welcome my server";

    //create a master socket
    if ((master_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    //set master socket to allow multiple connections , this is just a good habit, it will work without this
    if (setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *) &opt, sizeof(opt)) < 0) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    //type of socket created
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    //bind the socket to localhost port 5500
    if (bind(master_socket, (struct sockaddr *) &address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    printf("Listener on port %d \n", PORT);

    //try to specify maximum of 5 pending connections for the master socket
    if (listen(master_socket, 5) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    //accept the incoming connection
    addrlen = sizeof(address);
    poll_set[0].fd = master_socket;
    poll_set[0].events = POLLIN;
    while (TRUE) {
        int fd_index;
        int bytes_received;
        int client_sockfd;
        int total = 0;
        //for (int i = 1; i < MAX; i++) if (client[i] != 0) total++;
        //printf("Waiting for client (%d total)...\n", total);
        poll(poll_set, MAX, 10000);
        for (fd_index = 0; fd_index < MAX; fd_index++) {
            if (poll_set[fd_index].revents & POLLIN) {
                if (poll_set[fd_index].fd == master_socket) {
                    client_sockfd = accept(master_socket, (struct sockaddr *) &address, (socklen_t *) &addrlen);
                    for (int i = 1; i < MAX; i++) {
                        if (client[i] == 0) {
                            client[i] = client_sockfd;
                            poll_set[i].fd = client_sockfd;
                            poll_set[i].events = POLLIN;
                            status[i] = 0;
                            f[i] = fopen(get_file_name(i), "w+");
                            break;
                        }
                    }

                    printf("Adding client on fd %d\n", client_sockfd);
                    if (send(client_sockfd, message, strlen(message), 0) != strlen(message)) {
                        perror("send");
                    }
                } else {
                    ioctl(poll_set[fd_index].fd, FIONREAD, &bytes_received);
                    char buffer[1025];
                    int valread;
                    int sd = poll_set[fd_index].fd;
                    //Check if it was for closing , and also read the incoming message

                    if(status[fd_index] == 0){
                        if ((valread = recv( sd , buffer, 1024, 0)) != 1024)
                        {
                            buffer[valread] = '\0';
                            fputs(buffer, f[fd_index]);
                            status[fd_index] = 1;
                            rewind(f[fd_index]);
                            send(sd, "OK", 3, 0);
                        }
                        else
                        {
                            buffer[valread] = '\0';
                            fputs(buffer, f[fd_index]);

                        }
                    }
                    else if(status[fd_index] == 1){
                        valread = recv( sd , buffer, 1024, 0);
                        char buff[1024];
                        int count = fread(buff, sizeof(char), 1024, f[fd_index]);
                        buff[count] = '\0';
                        send(sd, buff, strlen(buff), 0);
                        if(count != 1024) status[fd_index] = 2;
                    }
                    else{
                        valread = recv( sd , buffer, 3, 0);
                        getpeername(sd , (struct sockaddr*)&address , (socklen_t*)&addrlen);
                        printf("Host disconnected , ip %s , port %d \n" , inet_ntoa(address.sin_addr) , ntohs(address.sin_port));
                        close( sd );
                        client[fd_index] = 0;
                        fclose(f[fd_index]);
                        status[fd_index] = 0;
                    }
                }
            }
        }
    }

    return 0;
}