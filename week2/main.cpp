#include <iostream>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>

using namespace std;

int IPToInfo(){
    string key;
    do{
        char ip_string[16];
        cout<< "-----------------------------" << endl;
        cout<< "----> IP to info" << endl;
        cout<< "----> Enter IP: ";
        cin >> ip_string;
        struct in_addr ip;
        if(inet_aton(ip_string, &ip) == 0){
            cout<< "IP khong hop le" << endl;
        }
        else{
            struct hostent *host;
            if((host = gethostbyaddr(&ip, sizeof(ip), AF_INET)) == NULL){
                cout<< "Khong tim thay thong tin" << endl;
            }
            else{
                cout<< "Domain: "<< host->h_name << endl;
                cout<< "Ip: " << ip_string << endl;

                cout<< "List alias:  " << endl;
                for(int i = 0; host->h_aliases[i] != NULL; i++) {
                    cout<< host->h_aliases[i] << endl;
                }

                cout<< "List ip:  " << endl;
                in_addr **list = (struct in_addr **) host->h_addr_list;
                for(int i = 0; list[i] != NULL; i++) {
                    cout<< inet_ntoa(*list[i]) << endl;
                }
            }
        }
        cout<< "Continue?(yes/n): ";
        cin >> key;
    }
    while(!key.compare("yes"));
}
int NameToInfo(){
    string key;
    do{
        char name_string[100];
        cout<< "-----------------------------" << endl;
        cout<< "----> Domain to info" << endl;
        cout<< "----> Enter domain: ";
        cin >> name_string;
        struct in_addr ip;
            struct hostent *host;
            if((host = gethostbyname(name_string)) == NULL){
                cout<< "Khong tim thaty thong tin" << endl;
            }
            else{
                in_addr **list = (struct in_addr **) host->h_addr_list;
                cout<< "Domain: "<< host->h_name << endl;
                cout<< "Ip: " << inet_ntoa(*list[0]) << endl;

                cout<< "List alias:  " << endl;
                for(int i = 0; host->h_aliases[i] != NULL; i++) {
                    cout<< host->h_aliases[i] << endl;
                }

                cout<< "List ip:  " << endl;

                for(int i = 0; list[i] != NULL; i++) {
                    cout<< inet_ntoa(*list[i]) << endl;
                }
            }
        cout<< "Continue?(yes/n): ";
        cin >> key;
    }
    while(!key.compare("yes"));
}

int main() {

    int key;
    do{
        cout<< "-----------------------------" << endl;
        cout<< "CHOCE" << endl;
        cout<< "1. IP to info" << endl;
        cout<< "2. Domain to info" << endl;
        cout<< "0. EXIT" << endl;
        cout<< "You choce?: ";
        cin >> key;
        switch(key){
            case 1: IPToInfo(); break;
            case 2: NameToInfo(); break;
        }
    }
    while(key != 0);
    return 0;
}