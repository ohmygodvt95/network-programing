//
// Created by lengkeng on 04/10/2016.
//
#include <iostream>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <zconf.h>
#include <sys/socket.h>
#include <sys/types.h>

using namespace std;
#define PORT 5500  /* Port that will be opened */
#define MAX 100

int main() {

    int server_sock; /* file descriptors */
    char recv_data[MAX];
    int bytes_sent, bytes_received;
    struct sockaddr_in server; /* server's address information */
    struct sockaddr_in client; /* client's address information */
    int sin_size;
    FILE *f;

    if ((server_sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {  /* calls socket() */
        printf("socket() error\n");
        exit(-1);
    }

    server.sin_family = AF_INET;
    server.sin_port = htons(PORT);   /* Remember htons() from "Conversions" section? =) */
    server.sin_addr.s_addr = INADDR_ANY;  /* INADDR_ANY puts your IP address automatically */
    bzero(&(server.sin_zero), 8); /* zero the rest of the structure */


    if (bind(server_sock, (struct sockaddr *) &server, sizeof(struct sockaddr)) == -1) { /* calls bind() */
        printf("bind() error\n");
        exit(-1);
    }


    while (1) {
        printf("\nWaiting...");
        f = fopen("tmp.txt", "w+");
        cout << "\nSaving..:)" << endl;
        long total = 0;
        sin_size = sizeof(struct sockaddr_in);

        while (bytes_received = recvfrom(server_sock, recv_data, MAX, 0, (struct sockaddr *) &client,
                                         (socklen_t *) &sin_size)) {
            recv_data[bytes_received] = '\0';
            for (int i = 0; i < strlen(recv_data); i++) {
                recv_data[i] = toupper(recv_data[i]);
            }
            fputs(recv_data, f);
            total += bytes_received;
            if (bytes_received != MAX) break;
        }
        rewind(f);
        cout << "Total: " << total << endl;

        char buff[MAX];
        int count = 0;
        cout << "Resending.." << endl;
        while (!feof(f)) {
            count = fread(buff, sizeof(char), MAX, f);
            buff[count] = '\0';
            bytes_sent = sendto(server_sock, buff, strlen(buff), 0, (struct sockaddr *) &client, sin_size);
            buff[0] = '\0';
        }
        printf("\nSUCCESS..\n");
        fclose(f);
    }
    close(server_sock);
    return 0;
}

