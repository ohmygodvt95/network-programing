//
// Created by lengkeng on 04/10/2016.
//
#include <iostream>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <zconf.h>
#include <sys/socket.h>
#include <sys/types.h>

using namespace std;
#define MAX 100
int main() {
    int client_sock;
    char buff[MAX];
    long total = 0;
    struct sockaddr_in server_addr;
    int bytes_sent, bytes_received, sin_size;
    int count = 0;
    FILE *f;
    char file_name[100];
    client_sock = socket(AF_INET, SOCK_DGRAM, 0);
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(5500);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    sin_size = sizeof(struct sockaddr);

    printf("\nPath file data:");
    gets(file_name);
    f = fopen(file_name, "r+");
    if (f == NULL) {
        printf("\nFile not exits");
        return 1;
    }
    printf("\nSending...\n");

    while (!feof(f)) {
        buff[0] = '\0';
        count = fread(buff, sizeof(char), MAX, f);
        buff[count] = '\0';

        bytes_sent = sendto(client_sock,buff,strlen(buff),0, (struct sockaddr *) &server_addr, sin_size);

        if(bytes_sent != MAX) break;
    }
    rewind(f);
    cout << "=>Success \nRecv \n" << endl;
    while(bytes_received = recvfrom(client_sock, buff, MAX, 0, (struct sockaddr *) &server_addr, (socklen_t *) &sin_size)){
        buff[bytes_received] = '\0';
        fputs(buff, f);
        total += bytes_received;
        if(bytes_received != MAX) break;
    }
    fclose(f);
    printf("\n->finish: %li bytes", total);
    close(client_sock);
    return 0;
}


