cmake_minimum_required(VERSION 3.6)
project(week4)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES ex1/client.cpp)
add_executable(ex1_client ${SOURCE_FILES})

set(SOURCE_FILES ex1/server.cpp)
add_executable(ex1_server ${SOURCE_FILES})

set(SOURCE_FILES ex2/client.cpp)
add_executable(ex2_client ${SOURCE_FILES})


set(SOURCE_FILES ex2/server.cpp)
add_executable(ex2_server ${SOURCE_FILES})