//
// Created by lengkeng on 12/10/2016.
//
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/wait.h>
#include <arpa/inet.h>

#define PORT 5500
#define BACKLOG 20

void sig_chld(int signo) {
    pid_t pid;
    int stat;
    while ((pid = waitpid(-1, &stat, WNOHANG)) > 0)
        printf("[ForkingServer] Child %d terminated\n", pid);
}

int main() {

    int listen_sock, conn_sock; /* file descriptors */
    char recv_data[1024];
    int bytes_sent, bytes_received;
    struct sockaddr_in server; /* server's address information */
    struct sockaddr_in client; /* client's address information */
    pid_t pid;
    int sin_size;

    if ((listen_sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {  /* calls socket() */
        printf("socket() error\n");
        exit(-1);
    }
    bzero(&server, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(PORT);   /* Remember htons() from "Conversions" section? =) */
    server.sin_addr.s_addr = htonl(INADDR_ANY);  /* INADDR_ANY puts your IP address automatically */

    if (bind(listen_sock, (struct sockaddr *) &server, sizeof(server)) == -1) { /* calls bind() */
        printf("bind() error\n");
        exit(-1);
    }

    if (listen(listen_sock, BACKLOG) == -1) {  /* calls listen() */
        printf("listen() error\n");
        exit(-1);
    }

    while (1) {
        sin_size = sizeof(struct sockaddr_in);
        if ((conn_sock = accept(listen_sock, (struct sockaddr *) &client, (socklen_t *) &sin_size)) ==
            -1) { /* calls accept() */
            printf("accept() error\n");
            exit(-1);
        }

        if ((pid = fork()) == 0) {
            FILE *f;
            close(listen_sock);
            printf("You got a connection from %s\n", inet_ntoa(client.sin_addr)); /* prints client's IP */
            bytes_sent = send(conn_sock, "Welcome to my server.\n", 22, 0); /* send to the client welcome message */
            if (bytes_sent < 0) {
                printf("\nError!Can not sent data to client!");
                close(conn_sock);
                continue;
            }
            f = fopen("tmp.txt", "w+");
            printf("\nSaving..");
            long total = 0;
            while (bytes_received = recv(conn_sock, recv_data, 1024, 0)) {
                recv_data[bytes_received] = '\0';

                for (int i = 0; i < strlen(recv_data); i++) recv_data[i] = toupper(recv_data[i]);
                fputs(recv_data, f);
                total += bytes_received;
                if (bytes_received != 1024) break;

            }

            rewind(f);
            printf("\n=>SUCCESS..: %li bytes", total);

            char buff[1024];
            int count = 0;
            printf("\nResending..");
            while (!feof(f)) {
                count = fread(buff, sizeof(char), 1024, f);
                buff[count] = '\0';
                bytes_sent = send(conn_sock, buff, strlen(buff), 0);
                buff[0] = '\0';
            }

            printf("\nSUCCESS..\n");
            fclose(f);

            exit(0);
        }

        signal(SIGCHLD, sig_chld);

        close(conn_sock);
    }
    close(listen_sock);
    return 1;
}


